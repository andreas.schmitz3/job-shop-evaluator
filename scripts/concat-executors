#!/usr/bin/env python
import os
from glob import glob
import argparse


__default_performance_metrics__ = [
    'metric_objective_time',
    'metric_mean_job_flow_time',
    'metric_mean_operation_flow_time',
    'metric_mean_operation_wait_time',
    'metric_mean_machine_idle_time_percentage'
]

__executor_infixes__ = [
    '*NoCommunication*',
    '*MachineTimeout*',
    '*OperationTimeout*',
    '*OperationCommunication*',
    '*Operator*',
]


def run():
    options = commandline_arguments()

    for metric in __default_performance_metrics__:
        for executor in __executor_infixes__:
            files = []
            for i in options.input:
                files += glob(os.path.join(i, metric, executor + ".yaml"))
            print(executor)
            if len(files) == 0:
                continue

            aggregated = []
            for f in files:
                print("    %s" % f)
                aggregated += get_filtered_file_content(f)
                # print(parsed)
            # print(aggregated)
            save_aggregated(aggregated, options.output, metric, executor.replace('*', ''))


def get_filtered_file_content(filename):
    with open(filename, 'r') as file:

        parsed = []
        state = 0
        for line in file:
            if state == 0:
                if line.startswith("---"):
                    state = 1
                else:
                    parts = line.split(" ")
                    parsed.append(" ".join(parts[1:]))

            elif state == 1:
                if line.startswith("---"):
                    state = 0

    return parsed


def save_aggregated(data, output_folder, metric, executor):
    preamble = [
        '---\n',
        'algname: %s\n' % executor,
        '---\n',
    ]
    for i, entry in enumerate(data):
        data[i] = "%d %s" % (i, entry)

    if output_folder and not os.path.exists(output_folder):
        os.makedirs(output_folder)

    metric_folder = os.path.join(output_folder, metric)
    if not os.path.isdir(metric_folder):
        os.mkdir(metric_folder)

    with open(os.path.join(metric_folder, executor + ".yaml"), 'w+') as outfile:
        outfile.writelines(preamble + data)


def commandline_arguments():
    """Parses the command line arguments and returns them bundled in an object
    that makes the values attribute accessible.

    Returns:
        Returns the parsed command line options

    """
    parser = argparse.ArgumentParser(
        description=""
    )
    parser.add_argument(
        'input',
        nargs='*',
    )

    parser.add_argument(
        '-o', '--output',
        help='Output folder of the concatenated files.',
        required=True
    )

    options = parser.parse_args()
    return options


if __name__ == "__main__":
    run()
