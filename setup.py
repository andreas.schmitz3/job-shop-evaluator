# -*- coding: utf-8 -*-

from setuptools import setup, find_packages
from os import path


# Get the long description from the README file
here = path.abspath(path.dirname(__file__))
with open(path.join(here, 'README.md'), encoding='utf-8') as f:
    long_description = f.read()


setup(
    name='job-shop-evaluator',
    version='1.0.0',  # NOTE: Also adapt in evaluator/__init__.py
    description='Evaluator tool for the job-shop-simulator: https://git.rwth-aachen.de/andreas.schmitz3/job-shop-simulator package.',
    long_description=long_description,
    author='Andreas Schmitz',
    author_email='andreas.schmitz3@rwth-aachen.de',
    url='https://git.rwth-aachen.de/andreas.schmitz3/job-shop-evaluator',
    license='GPL-3.0',
    packages=find_packages(exclude=['dependency-backup']),
    install_requires=[
        'appdirs~=1.4',
        'PyYAML~=3.12',
        'matplotlib~=2.0'
    ],
    dependency_links=[
        'git+https://github.com/ufpr-opt/perprof-py',
        'git+https://git.rwth-aachen.de/andreas.schmitz3/job-shop-simulator'
    ],
    scripts=[
        'scripts/job-shop-evaluator',
        'scripts/tikz-extractor',
        'scripts/concat-executors'
    ],
    # See https://pypi.python.org/pypi?%3Aaction=list_classifiers
    classifiers=[
        # How mature is this project? Common values are
        #   3 - Alpha
        #   4 - Beta
        #   5 - Production/Stable
        'Development Status :: 5 - Production/Stable',

        # Indicate who your project is intended for
        'Intended Audience :: Science/Research',
        'Topic :: Scientific/Engineering :: Information Analysis',

        # Environment
        'Environment :: Console',

        # Supported operating systems
        'Operating System :: Unix',
        'Operating System :: MacOS :: MacOS X',
        'Operating System :: POSIX :: Linux',

        # Pick your license as you wish (should match "license" above)
        'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',

        'Natural Language :: English',

        # Specify the Python versions you support here. In particular, ensure
        # that you indicate whether you support Python 2, Python 3 or both.
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: Implementation :: CPython',
    ],
        keywords='operations-research flexible-job-shop-scheduling simulation machine-scheduling evaluation',
)
