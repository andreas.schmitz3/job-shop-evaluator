# Job-Shop Scheduling Simulation Evaluator

This is the documentation of the ``job-shop-evaluator`` project. The repository of this project can be found here: [Job Shop Evaluator, RWTH Gitlab](https://git.rwth-aachen.de/andreas.schmitz3/job-shop-evaluator).

This project is a supplementary project to the ``job-shop-simulator`` project, which can be found here: [Job Shop Simulator, RWTH Gitlab](https://git.rwth-aachen.de/andreas.schmitz3/job-shop-simulator). This project is separated from the Job Shop Simulator due to licensing reasons. The project uses the `perprof-py` library which is licensed under the GPLv3. The main project, on the other hand, is licensed under MIT.

The ``job-shop-evaluator`` tool was created to start and evaluate multiple simulations without the need of human interaction. Furthermore, it uses the already mentioned `perprof-py` module to plot performance profiles of the tested simulations.

The project was developed for Python 3.6+ only, and it may not run properly with older Python 3 version. Most likely, it is not going to run with Python 2.7 at all.


## Installation

### Virtual Environment (Optional, Recommended)
If you want to use a virtual environment instead of installing the code globally, do:

  1. Create a Virtual Environment: ``python3.6 -m venv .venv``
  2. Activate environment: `source .venv/bin/activate` (To Deactivate environment run: deactivate)

Exit Virtual Environment

  1. `deactivate`


### Installation from Source
Install the distribution and all dependencies

  - ``pip install -r requirements.txt`` (recommend) or
  - ``python setup.py install`` (build dependencies may be necessary)

Depending on the selected `--plotting` option, further dependencies need to be installed. For example `--plotting tikz` additionally requires LaTeX and the `pdflatex` tool.


#### Linux
Most Linux versions bundle the Python 3.6 development files in a package that needs to be installed separately. It should be sufficient to install the packages: ``python3.6`` and ``python3.6-dev``.
It is also quite common that the virtual environment module is not part of the base installation, and part of a separate package, called  ``python3.6-venv``.

Furthermore, some Linux Python 3.6 installations do not pre-install the ``wheel`` Python package, which is required by some dependencies. Make sure that the ``wheel`` package is installed, e.g. by checking ``pip list``. It can be installed with ``pip install wheel``.

#### macOS
Simply install Python 3.6 with homebrew, via: ``brew install python3.6``.


### Dependency Problems

If the dependencies of the program could not be resolved for whatever reason, a tarball of every dependency can be found in the `dependency-backup` folder.

## Execution

  - Create a config file (see `config.yaml.sample`)
  - Start the evaluation with: ``job-shop-evaluator -c config.yaml ./output-folder``. For more information, see ``job-shop-evaluator --help``.
